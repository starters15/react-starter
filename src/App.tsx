import React, { useEffect } from 'react';

export const App = (props: { title: string }) => {
  useEffect(() => {
    document.title = props.title
  }, []);

  return (
    <h1>Hello from react starter</h1>
  );
}